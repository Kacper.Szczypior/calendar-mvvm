﻿using calendarSzczypior;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Calendar;
using NSubstitute;
using System.Linq;

namespace CalendarTests
{
    [TestClass]
    public class CalendarModelTests
    {
        [TestMethod]
        public void TestModel()
        {
            var modelGetter = Substitute.For<ICalendarModelLoader>();

            var model = new CalendarData();

            new List<CalendarEvent> {
                new CalendarEvent
                {
                    Begin = DateTime.Parse("2008-05-01"),
                    End = DateTime.Now.AddHours(1).TimeOfDay,
                    Title = "First Event"
                },
                new CalendarEvent
                {
                    Begin = DateTime.Parse("2008-05-01"),
                    End = DateTime.Now.AddHours(1).AddMinutes(1).TimeOfDay,
                    Title = "First Event"
                },
            }.ForEach(model.AddEvent);

            modelGetter.GetModel().Returns(model);



            var model2 = modelGetter.GetModel();

            var result = model2.GetForDay(DateTime.Parse("2008-05-01")).ToList();
            var result2 = model2.GetForDay(DateTime.Parse("2009-05-01")).ToList();

            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result2.Count, 0);
        }
    }
}
