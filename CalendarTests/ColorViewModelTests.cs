﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using calendarSzczypior;
using System.Windows.Media;

namespace CalendarTests
{
    [TestClass]
    public class ColorViewModelTests
    {
        [TestMethod]
        public void TestViewModelEventBeingRaised()
        {
            var raised = false;
            var model = new ColorViewModel();
            model.ColourChanged += (s, e) => raised = true;

            model.C = Colors.Black;

            Assert.AreEqual(raised, true);
        }
    }
}
