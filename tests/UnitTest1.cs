﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using calendarSzczypior;
using Moq;
using System.Collections.Generic;
using Calendar;

namespace tests
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void TestMethod1()
        {
            var mockDb = new Mock<IUserDBData>();
            var mockCalendarDB = new Mock<ICalendarData>();
            GlobalState.DetailsToBeOpened = 3;
            var us = new Person
            {
                Id = 1,
                Login = "1",
                PassWord = "2"
            };
            GlobalState.User = us;
            mockDb.Setup(x => x.GetUsers()).Returns(new List<Person>
            {
                us
            });

            mockCalendarDB.Setup(x => x.GetById(3)).Returns(new CalendarEvent
            {
                Title = "testEvent",
                Begin = DateTime.MaxValue,
                End = TimeSpan.MinValue,
                Inviter = us,
                EventVersion = 1,
                People = new List<Attendance>()
            });

            var model = new EventEditorViewModel(mockDb.Object, mockCalendarDB.Object);

            Assert.AreEqual(model.AdminRights, System.Windows.Visibility.Visible);
            Assert.AreEqual(model.EditEvent, System.Windows.Visibility.Visible);
            Assert.AreNotEqual(model.Decision, System.Windows.Visibility.Visible);
        }

        [TestMethod]
        public void TestUserAccepting()
        {
            var mockDb = new Mock<IUserDBData>();
            var mockCalendarDB = new Mock<ICalendarData>();
            GlobalState.DetailsToBeOpened = 3;
            var us = new Person
            {
                Id = 1,
                Login = "1",
                PassWord = "2"
            };
            var us2 = new Person
            {
                Id = 2,
                Login = "3",
                PassWord = "4"
            };
            GlobalState.User = us2;
            mockDb.Setup(x => x.GetUsers()).Returns(new List<Person>
            {
                us
            });

            mockCalendarDB.Setup(x => x.GetById(3)).Returns(new CalendarEvent
            {
                Title = "testEvent",
                Begin = DateTime.MaxValue,
                End = TimeSpan.MinValue,
                Inviter = us,
                EventVersion = 1,
                People = new List<Attendance>
                {
                    new Attendance
                    {
                        accepted = false,
                        Invitee = us2
                    }
                }
            });

            var model = new EventEditorViewModel(mockDb.Object, mockCalendarDB.Object);

            Assert.AreNotEqual(model.AdminRights, System.Windows.Visibility.Visible);
            Assert.AreNotEqual(model.EditEvent, System.Windows.Visibility.Visible);
            Assert.AreEqual(model.Decision, System.Windows.Visibility.Visible);
        }

        [TestMethod]
        public void DayNameContainer()
        {
            string dayName = "dayName";
            int row = 3;
            int column = 5;
            string css = "css";

            var container = new DayNameContainer(dayName, row, column, css);

            Assert.AreEqual(container.DayName, dayName);
            Assert.AreEqual(container.RowIndex, row);
            Assert.AreEqual(container.ColumndIndex, column);
            Assert.AreEqual(container.CssClass, css);
        }

        [TestMethod]
        public void TestFactory()
        {
            List<CalendarEvent> events = new List<CalendarEvent>
            {
                new CalendarEvent
                {
                    Title = "title"
                },
                new CalendarEvent
                {
                    Title = "title2"
                }
            };

            var date = DateTime.MinValue;
            var row = 3;
            var column = 5;

            var tested = ContainerFactory.GetEventContainer(events, date, row, column);

            Assert.AreEqual(tested.DayName, date.ToString());
            Assert.AreEqual(tested.RowIndex, row);
            Assert.AreEqual(tested.ColumndIndex, column);
            Assert.AreEqual(tested.Events.Count, events.Count);
        }
    }
}
