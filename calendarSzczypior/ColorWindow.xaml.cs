﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace calendarSzczypior
{
    /// <summary>
    /// Interaction logic for ColorWindow.xaml
    /// </summary>
    public partial class ColorWindow : Window
    {
        public ColorWindow(ColorViewModel model)
        {
            Model = model;
            InitializeComponent();
            cmbColors.ItemsSource = typeof(Colors).GetProperties();
            cmbColors.SelectedIndex = 1;
        }

        ColorViewModel Model { get; set; }

        private void CmbColors_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Handle();
        }

        private void CmbColors_DropDownClosed(object sender, System.EventArgs e)
        {
            Handle();
        }

        private void Handle()
        {
            var o = cmbColors.SelectedItem.ToString();
            var c = (Color)ColorConverter.ConvertFromString(o.Replace("System.Windows.Media.Color ", ""));
            Model.C = c;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            Model.Opened = false;
            base.OnClosing(e);
        }
    }
}
