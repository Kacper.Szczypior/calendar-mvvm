﻿namespace calendarSzczypior
{
    public interface ICalendarModelLoader{
        ICalendarData GetModel();

        void SaveModel(ICalendarData model);
    }
}
