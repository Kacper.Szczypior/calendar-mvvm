﻿using Calendar;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Windows;

namespace calendarSzczypior
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        WindowController c;
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var parameters = Environment.GetCommandLineArgs();

            var login = parameters[1];
            var password = parameters[2];

            try
            {
                var ctxt = new UserDBData();
                var id = ctxt.GetUser(login, password);
                GlobalState.User = id;
            }
            catch(Exception ex)
            {
                Shutdown();
            }

            c = new WindowController();
            var v = new MainWindow();
            v.Show();
        }
    }

    public class OpenDetailsCalendarMessage
    {
        public int id { get; set; }
    }

    public class OpenNewCalendarMessage
    {
        public DateTime date { get; set; }
    }

    public static class GlobalState
    {
        public static int DetailsToBeOpened;
        public static DateTime? DateToBeOpened = null;
        public static Person User;
    }

    public class RefreshMessage
    {

    }

    public class OpenColorSelector { }


    public class WindowController
    {
        public WindowController() {
            Messenger.Default.Register<OpenDetailsCalendarMessage>(this, OpenDetailsWindows);
            Messenger.Default.Register<OpenNewCalendarMessage>(this, OpenNewEventWindow);
        }

        public void OpenDetailsWindows(OpenDetailsCalendarMessage m)
        {
            GlobalState.DetailsToBeOpened = m.id;
            var window = new EventWindow();
            window.Show();
        }

        public void OpenNewEventWindow(OpenNewCalendarMessage m)
        {
            GlobalState.DateToBeOpened = m.date;
            var window = new EventWindow();
            window.Show();
        }

    }

}
