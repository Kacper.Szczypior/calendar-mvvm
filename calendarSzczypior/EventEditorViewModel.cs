﻿using Calendar;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace calendarSzczypior
{
    public class EventEditorViewModel : INotifyPropertyChanged
    {
        private readonly IUserDBData _userData;
        private readonly ICalendarData _calendarData;
        private string beginTime;
        private string calendarTitle;
        private string endTime;
        private Visibility errorMade = Visibility.Collapsed;
        private Visibility adminRights = Visibility.Collapsed;

        private Visibility decision = Visibility.Collapsed;

        private DateTime eventBegin;
        private int eventId;

        private int eventVersion;
        private readonly string TimeFormat = @"hh\:mm";

        public void AcceptEvent()
        {
            var l = Attendees.First(attend => attend.Invitee.Id == GlobalState.User.Id);
            l.accepted = true;
            var index = Attendees.IndexOf(l);
            Attendees.RemoveAt(index);
            Attendees.Insert(index, l);
            try
            {
                _calendarData.AcceptInvitation(l, eventVersion);
            }
            catch (EventConcurrencyException ex)
            {
                ErrorMade = Visibility.Visible;
                ErrorMessage = "Someone else edited this event";
            }
        }

        public void DenyEvent()
        {
            var l = Attendees.First(attend => attend.Invitee.Id == GlobalState.User.Id);
            l.accepted = false;
            var index = Attendees.IndexOf(l);
            Attendees.RemoveAt(index);
            Attendees.Insert(index, l);
            try
            {
                _calendarData.DenyInvitation(l, eventVersion);
            }
            catch(EventConcurrencyException ex)
            {
                ErrorMade = Visibility.Visible;
                ErrorMessage = "Someone else edited this event";
            }
        }

        public void RemoveInvitee(int id)
        {
            for(int i = Attendees.Count - 1; i >= 0; i--)
            {
                if(Attendees[i].Id == id)
                {
                    Attendees.RemoveAt(i);
                    break;
                }
            }
        }

        public void AddInvitee()
        {
            if(SelectedInvitee.Id == Inviter.Id)
            {
                return;
            }

            if (Attendees.Any(atte => atte.Invitee.Id == SelectedInvitee.Id))
            {
                return;
            }
            var att = new Attendance
            {
                Invitee = SelectedInvitee,
                accepted = false
            };
            Attendees.Add(att);
        }

        public EventEditorViewModel(IUserDBData data, ICalendarData calendardata)
        {
            _userData = data;
            _calendarData = calendardata;
            var p = _userData.GetUsers();

            foreach (var person in p)
            {
                Persons.Add(person);
            }

            SelectedInvitee = Persons[0];
            if (GlobalState.DetailsToBeOpened != 0)
            {
                NewEvent = Visibility.Hidden;

                eventId = GlobalState.DetailsToBeOpened;
                GlobalState.DetailsToBeOpened = 0;
                var calendarEvent = _calendarData.GetById(eventId);

                CalendarTitle = calendarEvent.Title;
                EventBegin = calendarEvent.Begin.Date;
                BeginTime = calendarEvent.Begin.TimeOfDay.ToString(TimeFormat);
                EndTime = calendarEvent.End.ToString(TimeFormat);
                eventVersion = calendarEvent.EventVersion;
                Inviter = calendarEvent.Inviter;

                foreach (var attend in calendarEvent.People)
                {
                    Attendees.Add(attend);
                    if (attend.Invitee.Id == GlobalState.User.Id)
                    {
                        Decision = Visibility.Visible;
                    }
                }
                if (calendarEvent.Inviter.Id == GlobalState.User.Id)
                {
                    EditEvent = Visibility.Visible;
                    AdminRights = Visibility.Visible;
                }
                else
                {
                    EditEvent = Visibility.Hidden;
                }

            }
            else if (GlobalState.DateToBeOpened.HasValue)
            {
                EventBegin = GlobalState.DateToBeOpened.Value;
                GlobalState.DateToBeOpened = null;
                NewEvent = Visibility.Visible;
                EditEvent = Visibility.Collapsed;
                BeginTime = DateTime.Now.TimeOfDay.ToString(TimeFormat);
                EndTime = DateTime.Now.TimeOfDay.ToString(TimeFormat);
                Inviter = GlobalState.User;
                AdminRights = Visibility.Visible;
            }
            else
            {
                NewEvent = Visibility.Visible;
                EditEvent = Visibility.Hidden;
            }
        }

        public EventEditorViewModel() : this(new UserDBData(), new CalendarDBData())
        {
        }

        private string message = "Wrong Date Format";
        public string ErrorMessage { get { return message; } set { message = value; OnPropertyChanged(nameof(ErrorMessage)); } }

        public event PropertyChangedEventHandler PropertyChanged;
        public string BeginTime { get { return beginTime; } set { beginTime = value; } }
        public string CalendarTitle { get { return calendarTitle; }  set { calendarTitle = value; } }
        public Visibility concurrencyErrorMade { get; set; } = Visibility.Hidden;
        public Visibility ConcurrencyErrorMade { get { return concurrencyErrorMade; } set { concurrencyErrorMade = value; OnPropertyChanged(nameof(ConcurrencyErrorMade)); } }
        public Visibility EditEvent { get; }
        public string EndTime { get { return endTime; } set { endTime = value; } }
        public Visibility ErrorMade { get { return errorMade; } set { errorMade = value; OnPropertyChanged(nameof(errorMade)); } }
        public DateTime EventBegin { get { return eventBegin; } set { eventBegin = value; } }
        public Visibility NewEvent { get; }

        public ObservableCollection<Person> Persons { get; set; } = new ObservableCollection<Person>();
        public ObservableCollection<Attendance> Attendees { get; set; } = new ObservableCollection<Attendance>();

        public Person SelectedInvitee { get; set; }
        public Person Inviter { get; set; }

        public Visibility AdminRights { get { return adminRights; } set { adminRights = value; OnPropertyChanged(nameof(adminRights)); } }

        public Visibility Decision { get { return decision; } set { decision = value; OnPropertyChanged(nameof(decision)); } }

        public bool AddNewEvent()
        {
            errorMade = Visibility.Hidden;
            TimeSpan beg, end;
            if (TimeSpan.TryParse(beginTime, out beg) && TimeSpan.TryParse(endTime, out end))
            {
                if (!string.IsNullOrEmpty(CalendarTitle))
                {
                    var data = new CalendarDBData();

                    var invited = new Collection<Attendance>();
                    foreach (var att in Attendees)
                    {
                        invited.Add(att);
                    }

                    data.AddEvent(new CalendarEvent
                    {
                        Begin = EventBegin.Date.Add(beg),
                        End = end,
                        Title = CalendarTitle,
                        EventVersion = eventVersion,
                        Inviter = GlobalState.User,
                        People = invited
                    });
                    Messenger.Default.Send(new RefreshMessage());
                    return true;
                }
                ErrorMade = Visibility.Visible;
                ErrorMessage = "Empty event title";
                return false;
            }
            ErrorMade = Visibility.Visible;
            ErrorMessage = "Wrong date format";
            return false;
        }

        public bool RemoveEvent()
        {
            try
            {
                _calendarData.RemoveById(eventId, eventVersion);
            }
            catch(EventConcurrencyException ex)
            {
                ErrorMade = Visibility.Visible;
                ErrorMessage = "Someone else edited this event";
                return false;
            }
            Messenger.Default.Send(new RefreshMessage());
            return true;
        }

        public bool ValidateEditEvent()
        {
            ErrorMade = Visibility.Hidden;
            TimeSpan beg, end;
            if (TimeSpan.TryParse(beginTime, out beg) && TimeSpan.TryParse(endTime, out end))
            {
                if (!string.IsNullOrEmpty(CalendarTitle))
                {
                    try
                    {
                        var data = new CalendarDBData();
                        var invited = new Collection<Attendance>();
                        foreach (var att in Attendees)
                        {
                            invited.Add(att);
                        }
                        data.EditEvent(eventId, new CalendarEvent
                        {
                            Begin = EventBegin.Date.Add(beg),
                            End = end,
                            Title = CalendarTitle,
                            EventVersion = eventVersion,
                            People = invited
                        });
                        Messenger.Default.Send(new RefreshMessage());
                        return true;
                    }
                    catch (EventConcurrencyException e)
                    {
                        ErrorMade = Visibility.Visible;
                        ErrorMessage = "Someone else edited this event";
                        return false;
                    }
                }
            }
            ErrorMade = Visibility.Visible;
            return false;
        }
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
