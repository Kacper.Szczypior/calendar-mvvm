﻿using Calendar;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace calendarSzczypior
{
    public class Day
    {
        public Day(string title, int eventId)
        {
            WeirdStringName = title;
            EventId = eventId.ToString();
        }

        public string WeirdStringName { get; set; }
        public string EventId { get; set; }
    }

    public class ContainerFactory
    {
        public static DayNameContainer GetEventContainer(List<CalendarEvent> events, DateTime date, int rowIndex, int columnIndex)
        {
            var list = events.Where(e => e.Begin.Date == date.Date);

            var container = new DayNameContainer();
            container.RowIndex = rowIndex;
            container.ColumndIndex = columnIndex;
            container.DayName = date.Date.ToString();

            foreach (var ele in list)
            {
                container.Events.Add(new Day(ele.Title, ele.Id));
            }

            return container;
        }
    }

    public class DayNameContainer
    {
        public DayNameContainer(string dayName, int rowIndex, int columnIndex, string cssClass)
        {
            DayName = dayName;
            RowIndex = rowIndex;
            ColumndIndex = columnIndex;
            CssClass = cssClass;
            IsDayWeek = true;
        }

        public DayNameContainer() { }

        public string CssClass { get; set; }
        public string DayName { get; set; }
        public int RowIndex { get; set; }
        public int ColumndIndex { get; set; }
        public bool IsDayWeek { get; set; }
        public bool IsDateLabel { get; set; }
        public bool IsEventLabel { get; set; }
        public ObservableCollection<Day> Events { get; set; } = new ObservableCollection<Day>();
    }

    public class CalendarViewModel
    {
        private const int DaysInWeek = 7;
        private const int ShownWeeeks = 4;
        public CalendarViewModel()
        {

            Model = new CalendarDBData();

            Today = DateTime.Now;
            if (Today.DayOfWeek != DayOfWeek.Sunday)
                Today = Today.AddDays(-(int)Today.DayOfWeek);

            Messenger.Default.Register<RefreshMessage>(this, Refresh);

            DrawDays();
        }

        public void OpenColorSelector()
        {

        }

        public ObservableCollection<DayNameContainer> GridData { get; set; } = new ObservableCollection<DayNameContainer>();
        public ICalendarData Model { get; set; }

        public DateTime Today { get; set; }


        public void OpenDetails(int Id)
        {
            Messenger.Default.Send(new OpenDetailsCalendarMessage { id = Id });
        }

        public void OpenDetails(DateTime time)
        {
            Messenger.Default.Send(new OpenNewCalendarMessage { date = time });
        }

        public void MoveForward()
        {
            Today = Today.AddDays(7);

            Refresh();
        }

        private void Refresh(RefreshMessage m = null)
        {
            for (int i = GridData.Count - 1; i >= 0; i--)
            {
                DateTime result;
                if (DateTime.TryParse(GridData[i].DayName, out result))
                {
                    GridData.RemoveAt(i);
                }
            }

            DrawChangeAbleStaff();
        }

        public void MoveBackWard()
        {
            Today = Today.AddDays(-7);
            Refresh();
        }

        private void DrawDays()
        {
            Enumerable.Range(0, DaysInWeek)
                .Select(i => new { gridIndex = i + 1, dayName = ((DayOfWeek)((i + 6) % 7)).ToString() })
                .ToList()
                .ForEach(u =>
                {
                    GridData.Add(new DayNameContainer(u.dayName, 0, u.gridIndex, "DayOfWeekStyle"));
                    GridData.Add(new DayNameContainer(u.dayName, ShownWeeeks + 1, u.gridIndex, "DayOfWeekStyle"));
                });

            GridData.Add(new DayNameContainer()
            {
                ColumndIndex = DaysInWeek + 1,
                RowIndex = 0,
                DayName = "Next",
            });
            GridData.Add(new DayNameContainer()
            {
                ColumndIndex = 0,
                RowIndex = 0,
                DayName = "Next"
            });
            GridData.Add(new DayNameContainer()
            {
                ColumndIndex = DaysInWeek + 1,
                RowIndex = ShownWeeeks + 1,
                DayName = "Previous"
            });
            GridData.Add(new DayNameContainer()
            {
                ColumndIndex = 0,
                RowIndex = ShownWeeeks + 1,
                DayName = "Previous"
            });





            DrawChangeAbleStaff();
        }

        private void DrawChangeAbleStaff()
        {
            var tmpDate = Today;
            var finalDate = Today.AddDays(DaysInWeek * ShownWeeeks - 1);
            var events = Model.GetBetweenDatesIncluding(tmpDate, finalDate, GlobalState.User.Id).ToList();
            for (int j = 0; j < ShownWeeeks; j++)
            {
                var weekNumber = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(tmpDate, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
                GridData.Add(new DayNameContainer($"W{weekNumber}", j + 1, 0, "DayOfWeekStyle"));
                GridData.Add(new DayNameContainer($"W{weekNumber}", j + 1, DaysInWeek + 1, "DayOfWeekStyle"));

                for (int i = 0; i < DaysInWeek; i++, tmpDate = tmpDate.AddDays(1))
                {
                    var elo2 = ContainerFactory.GetEventContainer(events.Where(e => e.Begin.Date == tmpDate.Date).ToList(), tmpDate.Date, j + 1, i + 1);
                    GridData.Add(elo2);
                }
            }
        }
    }
}
