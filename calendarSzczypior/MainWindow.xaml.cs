﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace calendarSzczypior
{
    public partial class MainWindow : Window
    {
        private readonly double FontModifier = 0.02;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void CalendarGrid_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //if (e.ClickCount == 2)
            //{
            //    var panel = sender as Panel;
            //    var date = Today.AddDays(int.Parse(panel.Name.Replace("day", "")));
            //    ShowEventModal("", date, DateTime.Now.TimeOfDay, DateTime.Now.TimeOfDay, EventEditorMode.create, 0);
            //}
        }

        //private void HandleColourChanged(object sender, EventArgs e)
        //{
        //    var style = new Style
        //    {
        //        TargetType = typeof(Label),
        //    };
        //    style.Setters.Add(new Setter(Label.BackgroundProperty, new SolidColorBrush(ColorViewModel.C)));

        //    Resources["DayStyle"] = style;
        //}

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            FontSize = ActualWidth * FontModifier;
        }

        //private void Window_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        //{
        ////    if (ColorViewModel.Opened)
        ////        return;
        ////    ColorViewModel.Opened = true;
        ////    var colorChooser = new ColorWindow(ColorViewModel);
        ////    colorChooser.Show();
        //}

        private void Label_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var localsender = (Label)sender;

            var tag = localsender.Tag as string;

            int Id;
            if (int.TryParse(tag, out Id))
            {
                VM.OpenDetails(Id);
                e.Handled = true;
            }
        }

        private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var localsender = (StackPanel)sender;

            var tag = localsender.Tag as string;

            if(tag == "Next")
            {
                VM.MoveForward();
            }
            if(tag == "Previous")
            {
                VM.MoveBackWard();
            }
            if(e.ClickCount == 2)
            {
                DateTime result;
                if(DateTime.TryParse(tag, out result))
                {
                    VM.OpenDetails(result);
                }
            }
        }

        private CalendarViewModel VM { get { return Resources["ViewModel"] as CalendarViewModel; } }
    }
}
