﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace calendarSzczypior
{
    public class CalendarModelLoader : ICalendarModelLoader
    {
        private static readonly string ModelPath = "model.txt";

        public ICalendarData GetModel()
        {
            Logger.log.Debug("loading model");
            try
            {
                using (var stream = File.Open(ModelPath, FileMode.Open))
                {
                    var binFormatter = new BinaryFormatter();
                    var model = (ICalendarData)binFormatter.Deserialize(stream);
                    return model;
                }
            }
            catch(Exception ex)
            {
                return new CalendarData();
            }
        }

        public void SaveModel(ICalendarData model)
        {
            Logger.log.Debug("saving model");
            var toSerialize = (CalendarData)model;
            using (var stream = File.Open(ModelPath, FileMode.Create))
            {
                var binFormatter = new BinaryFormatter();
                binFormatter.Serialize(stream, toSerialize);
            }
        }
    }
}
