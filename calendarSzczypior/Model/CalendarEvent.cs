﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Calendar
{
    public class CalendarEventContext : DbContext
    {
        public DbSet<CalendarEvent> Events { get; set; }

        public DbSet<Person> Persons { get; set; }

        public DbSet<Attendance> Attendance { get; set; }
    }

    public class Attendance
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public Person Invitee { get; set; }
        public bool accepted { get; set; }
        public CalendarEvent Attend { get; set; }
    }

    public class Person
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        [MinLength(3)]
        [StringLength(300)]
        [Index(IsUnique = true)]
        public string Login { get; set; }
        [Required]
        [MinLength(8)]
        public string PassWord { get; set; }

        public ICollection<Attendance> Attendances { get; set; }
    }

    [Serializable]
    public class CalendarEvent
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public DateTime Begin { get; set; }
        [Required]
        public TimeSpan End { get; set; }
        [Required]
        public string Title { get; set; }

        [Required]
        public Person Inviter { get; set; }

        public int EventVersion { get; set; }

        [Required]
        public virtual ICollection<Attendance> People { get; set; }
    }
}
