﻿using System.Collections.Generic;
using Calendar;

namespace calendarSzczypior
{
    public interface IUserDBData
    {
        Person GetUser(string login, string password);
        IEnumerable<Person> GetUsers();
    }
}