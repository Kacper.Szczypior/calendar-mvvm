﻿using Calendar;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace calendarSzczypior
{
    public class UserDBData : IUserDBData
    {
        public IEnumerable<Person> GetUsers()
        {
            using (var db = new CalendarEventContext())
            {
                return db.Persons.ToList();
            }
        }

        public Person GetUser(string login, string password)
        {
            Logger.log.Debug("validating user");
            using (var db = new CalendarEventContext())
            {
                var count = db.Persons.Count();

                if(count < 5)
                {
                    db.Persons.Add(new Person { Login = "user1", PassWord = "password" });
                    db.Persons.Add(new Person { Login = "user2", PassWord = "password" });
                    db.Persons.Add(new Person { Login = "user3", PassWord = "password" });
                    db.Persons.Add(new Person { Login = "user4", PassWord = "password" });
                    db.Persons.Add(new Person { Login = "user5", PassWord = "password" });
                }

                db.SaveChanges();

                return db.Persons.First(p => p.Login == login && p.PassWord == password);
            }
        }
    }

    public class EventConcurrencyException : Exception
    {

    }

    public class CalendarDBData : ICalendarData
    {
        public void RemoveInvitee(int id, int token)
        {
            using (var db = new CalendarEventContext())
            {
                var att = new Attendance
                {
                    Id = id
                };

                db.Attendance.Attach(att);
                db.Attendance.Remove(att);

                db.SaveChanges();
            }
        }

        public void AddEvent(CalendarEvent e)
        {
            e.EventVersion = 1;
            Logger.log.Debug("loading model");
            using (var db = new CalendarEventContext())
            {
                var persons = e.People;
                foreach(var p in persons)
                {
                    var dbperson = db.Persons.First(person => person.Login == p.Invitee.Login);
                    p.Invitee = dbperson;
                }

                e.Inviter = db.Persons.First(person => person.Login == e.Inviter.Login);

                db.Events.Add(e);                
                db.SaveChanges();
            }
        }

        public void EditEvent(int id, CalendarEvent e)
        {
            Logger.log.Debug("loading model");
            using (var db = new CalendarEventContext())
            {
                var original = db.Events.Find(id);
                if(original == null)
                {
                    throw new EventConcurrencyException();
                }
                db.Entry(original).Collection(ori => ori.People).Load();

                if (original.EventVersion != e.EventVersion)
                    throw new EventConcurrencyException();

                original.EventVersion++;
                original.Begin = e.Begin;
                original.End = e.End;
                original.Title = e.Title;

                var itemsToDelete = original.People.ToList();
                db.Attendance.RemoveRange(itemsToDelete);

                db.SaveChanges();
            }

            using (var db = new CalendarEventContext())
            {
                var original = db.Events.Find(id);
                foreach (var p in e.People)
                {
                    var l = new Attendance
                    {
                        accepted = p.accepted,
                    };
                    var dbperson = db.Persons.First(person => person.Login == p.Invitee.Login);
                    l.Invitee = dbperson;
                    original.People.Add(l);
                    
                }

                db.SaveChanges();
            }
        }

        public CalendarEvent GetById(int id)
        {
            Logger.log.Debug("loading model");
            using (var db = new CalendarEventContext())
            {
                var toret = db.Events.Find(id);
                db.Entry(toret).Collection(p => p.People).Load();
                db.Entry(toret).Reference(p => p.Inviter).Load();
                foreach(var e in toret.People)
                {
                    db.Entry(e).Reference(k => k.Invitee).Load();
                }

                return toret;
            }
        }

        public IEnumerable<CalendarEvent> GetBetweenDatesIncluding(DateTime begin, DateTime end, int userid)
        {
            Logger.log.Debug("loading model");
            using (var db = new CalendarEventContext())
            {
                var rule = Convert.ToDateTime(begin.Date);
                var rule2 = Convert.ToDateTime(end.Date);
                return db.Events
                    .Where(u => DbFunctions.TruncateTime(u.Begin) >= rule && DbFunctions.TruncateTime(u.Begin) <= end.Date &&
                    (u.People.Any(p => p.Invitee.Id == userid) || u.Inviter.Id == userid))
                    .OrderBy(e => e.Begin)
                    .ToList();
            }
        }

        public void RemoveById(int id, int token)
        {
            Logger.log.Debug("loading model");
            using (var db = new CalendarEventContext())
            {
                CalendarEvent eve;
                try
                {
                    eve = db.Events.First(s => s.Id == id);
                    if (eve.EventVersion != token)
                    {
                        throw new Exception();
                    }
                }
                catch (Exception ex)
                {
                    throw new EventConcurrencyException();
                }



                db.Entry(eve).Reference(k => k.Inviter).Load();
                db.Entry(eve).Collection(k => k.People).Load();

                foreach(var h in eve.People)
                {
                    db.Entry(h).Reference(k => k.Invitee).Load();
                }

                db.Events.Remove(eve);

                db.SaveChanges();
            }
        }

        public void AddInvitee(Attendance att, int EventId)
        {
            using (var ctxt = new CalendarEventContext())
            {
                var eve = ctxt.Events.Where(e => e.Id == EventId).First();

                eve.People.Add(att);

                ctxt.SaveChanges();
            }
        }

        public void AcceptInvitation(Attendance e, int token)
        {
            using (var ctxt = new CalendarEventContext())
            {
                try
                {
                    var origin = ctxt.Events.First(s => s.People.Any(l => l.Id == e.Id));
                    if (origin.EventVersion != token)
                    {
                        throw new Exception();
                    }
                }
                catch (Exception ex)
                {
                    throw new EventConcurrencyException();
                }
                var toAccept = ctxt.Attendance.First(k => k.Id == e.Id);
                toAccept.accepted = true;
                ctxt.SaveChanges();
            }
        }

        public void DenyInvitation(Attendance e, int token)
        {
            using (var ctxt = new CalendarEventContext())
            {
                try
                {
                    var origin = ctxt.Events.First(s => s.People.Any(l => l.Id == e.Id));
                    if (origin.EventVersion != token)
                    {
                        throw new Exception();
                    }
                }
                catch(Exception ex)
                {
                    throw new EventConcurrencyException();
                }
                
                var toAccept = ctxt.Attendance.First(k => k.Id == e.Id);
                toAccept.accepted = false;
                ctxt.SaveChanges();
            }
        }
    }
}
