namespace calendarSzczypior.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addusers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CalendarEvents", "UserName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CalendarEvents", "UserName");
        }
    }
}
