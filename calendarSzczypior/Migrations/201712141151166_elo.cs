namespace calendarSzczypior.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class elo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attendances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        accepted = c.Boolean(nullable: false),
                        Attend_Id = c.Int(),
                        Invitee_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CalendarEvents", t => t.Attend_Id)
                .ForeignKey("dbo.People", t => t.Invitee_Id, cascadeDelete: true)
                .Index(t => t.Attend_Id)
                .Index(t => t.Invitee_Id);
            
            CreateTable(
                "dbo.People",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Login = c.String(nullable: false, maxLength: 300),
                        PassWord = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Login, unique: true);
            
            AddColumn("dbo.CalendarEvents", "EventVersion", c => c.Int(nullable: false));
            AddColumn("dbo.CalendarEvents", "Inviter_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.CalendarEvents", "Title", c => c.String(nullable: false));
            CreateIndex("dbo.CalendarEvents", "Inviter_Id");
            AddForeignKey("dbo.CalendarEvents", "Inviter_Id", "dbo.People", "Id", cascadeDelete: true);
            DropColumn("dbo.CalendarEvents", "UserName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CalendarEvents", "UserName", c => c.String());
            DropForeignKey("dbo.Attendances", "Invitee_Id", "dbo.People");
            DropForeignKey("dbo.Attendances", "Attend_Id", "dbo.CalendarEvents");
            DropForeignKey("dbo.CalendarEvents", "Inviter_Id", "dbo.People");
            DropIndex("dbo.People", new[] { "Login" });
            DropIndex("dbo.CalendarEvents", new[] { "Inviter_Id" });
            DropIndex("dbo.Attendances", new[] { "Invitee_Id" });
            DropIndex("dbo.Attendances", new[] { "Attend_Id" });
            AlterColumn("dbo.CalendarEvents", "Title", c => c.String());
            DropColumn("dbo.CalendarEvents", "Inviter_Id");
            DropColumn("dbo.CalendarEvents", "EventVersion");
            DropTable("dbo.People");
            DropTable("dbo.Attendances");
        }
    }
}
