﻿using System.Windows;
using System.Windows.Controls;

namespace calendarSzczypior
{
    public partial class EventWindow : Window
    {
        public bool BCreateNewEvent { get; set; }
        public bool BEditEvent { get; set; }
        public bool BRemoveEvent { get; set; }
        private EventEditorViewModel VM { get { return Resources["ViewModel"] as EventEditorViewModel; } }


        public EventWindow()
        {
            InitializeComponent();
        }


        private void NewEvent_Click(object sender, RoutedEventArgs e)
        {
            if (VM.AddNewEvent())
                Close();
        }

        private void Add_Invitee(object sender, RoutedEventArgs e)
        {
            VM.AddInvitee();
        }

        private void EditEvent_Click(object sender, RoutedEventArgs e)
        {
            if (VM.ValidateEditEvent())
                Close();
        }

        private void RemoveEvent_Click(object sender, RoutedEventArgs e)
        {
            if (VM.RemoveEvent())
                Close();
        }

        private void RemoveInvitee_Click(object sender, RoutedEventArgs e)
        {
            var tag = ((Button)sender).Tag.ToString();
            var id = int.Parse(tag);


            VM.RemoveInvitee(id);
        }

        private void Accept_Click(object sender, RoutedEventArgs e)
        {
            VM.AcceptEvent();
        }

        private void Deny_Click(object sender, RoutedEventArgs e)
        {
            VM.DenyEvent();
        }
    }
}
