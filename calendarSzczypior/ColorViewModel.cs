﻿using System;
using System.Windows.Media;

namespace calendarSzczypior
{
    public class ColorViewModel
    {
        private Color c;
        public bool Opened = false;

        public event EventHandler ColourChanged;
        public Color C { get { return c; } set { c = value; ColourChanged?.Invoke(this, EventArgs.Empty); } }
    }
}
