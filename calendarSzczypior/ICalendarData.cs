﻿using Calendar;
using System;
using System.Collections.Generic;

namespace calendarSzczypior
{
    public interface ICalendarData
    {
        void AcceptInvitation(Attendance e, int token);
        void DenyInvitation(Attendance e, int token);
        void AddEvent(CalendarEvent e);
        void EditEvent(int id, CalendarEvent e);
        CalendarEvent GetById(int id);
        void RemoveById(int id, int token);
        IEnumerable<CalendarEvent> GetBetweenDatesIncluding(DateTime begin, DateTime end, int UserId);
        void RemoveInvitee(int id, int token);
        void AddInvitee(Attendance att, int EventId);
    }
}
